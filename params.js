module.exports=function(){
	return {
		hourlyWage:3.75,
		currency:'$',
		evening:{
			start:'18:00',
			end:'06:00',
			compensation:'1.15'
		},
		regularWorkingHours: 8,
		overtime:[{
			hours:4, //if the overtime work is done for > 4 hours, compensation is 0.25 of the regular hourlywage
			rate:1
		},
		{
			hours:2,
			rate:0.50
		},
		{
			hours:0,
			rate:0.25
			
		}],
		dateTimeFormat:'DD-M-YYYY"T"HH:mm',
		dateFormat:'DD-M-YYYY',
		timeFormat:'HH:mm',
		csv:{
			column:{
				name:'Person Name',
				id:'Person ID',
				date:'Date',
				start:'Start',
				end:'End'
			}
		}
	};
}