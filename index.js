
var _ = require('underscore'),
    Slot = require('./documents/slot.js'),
    dateUtil = require('./utils/dateUtil.js'),
    LineByLineReader = require('line-by-line'),
    wageCalculator = require('./calculators/wageCalculator.js');

var arguments = process.argv.splice(2);
var dir = arguments[0] || './csv/HourList201403.csv';

var lr = new LineByLineReader(__dirname+'/'+dir),
    idx = 0,
    firstRow = '',
    slots = [];
	
lr.on('error', function(err){
	var error ={
		message:'File not found, please check the path carefully',
		path:err.path
	};
	console.log(error);
});
lr.on('line', function(line){
	idx++ ;
	if(idx===1){
		firstRow = line;
	}else if(idx>1){
		var slot = new Slot(line, firstRow);
		slots.push(slot); // add all slots data to an array
		
	}
});
lr.on('end', function(){
	//group the slots by id
	var groupedSlots = _.groupBy(slots, function(slot){
		return slot.id;
	});
	var ids = _.keys(groupedSlots);
	var month = dateUtil.getMonth(slots[0].date, 'MM/YYYY');
	console.log('Monthly Wages '+ month+':');
	console.log('id,name,total wage,evening compensation,overtime compensation');
	_.map(ids, function(id){
		var report = wageCalculator.getWageReport(groupedSlots[id]);
		console.log(report.toString());
	});
	
});