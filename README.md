This repo contains the implementation of Solinor's coding exercise : Monthly Wage Calculation System

**Quick summary
**
It is implemented with Node.js, the output is displayed in the terminal shown in the figure below.

![Screen Shot 2015-02-22 at 19.56.09.png](https://bitbucket.org/repo/nA5y6K/images/3236352537-Screen%20Shot%202015-02-22%20at%2019.56.09.png)

**Prerequisite:
**
Node.js, version for development is v0.10.34

**How to run:
**
After cloning the project, cd to the project directory, to install dependencies use 'npm install'.
Command to run the project is 

node index.js [path]

[path] is optional, you should use a relative path to the root directory of the project. E.g. The sample cvs file is located in csv folder, so path is ./csv/HourList201403.csv, you should run it using node index.js ./csv/HourList201403.csv.

When [path] is absent, the default value for path is set to ./csv/HourList201403.csv.

The command to run the test is mocha, you should run it in the project directory.

**Dependencies**
underscore.js, moment.js and line-by-line.js

**Dev dependencies** 
mocha.js, chai.js