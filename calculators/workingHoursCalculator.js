//TODO move the configs to another file
var _ = require('underscore'),
	strategy = require('../algorithms/wageCalculationStrategies.js'),
	util = require('../utils/dateUtil.js');

	function getDailyWorkingHours(dailySlots){
		var hour={
			total:0.00,
			evening:0.00,
			overtime:0.00
		};
		var daily= _.reduce(dailySlots, function(m, dailySlot){
			
			m.total += util.getDuration(dailySlot.date, dailySlot.start, dailySlot.end);
			m.evening += util.getEveningHours(dailySlot.date, dailySlot.start, dailySlot.end);
			return m;
		},hour);
		daily.overtime = util.getOverTimeDuration(daily.total);
		return daily;
	}
	/**
 	*This module defines functions to calculate working hours
 	*/
	module.exports = {
		/**
		 *get the sum of working hours in the slots array, including total, evening and overtime hours
		 *@param slots, array of objects ({id:'',name:'', date:'', from:'', until:''}) from the same ID
		 *@return object, 
		 */
		getWorkingHours : function(slots){
		
			var groupedSlots = _.groupBy(slots, function(slot){
				return slot.date;
			});
			var days = _.keys(groupedSlots);
			var intial = {
				total:0.00,
				evening:0.00,
				overtime:[] //overtime compensation depends on duration of each day, so it has to be separated based on date
			};
			return  _.reduce(days, function(memo, day){
				var dailySlots = groupedSlots[day];
				var dailyWorkingHour = getDailyWorkingHours(dailySlots);
				memo.total+=dailyWorkingHour.total;
				memo.evening += dailyWorkingHour.evening;
				if(dailyWorkingHour.overtime>0){
					memo.overtime.push(dailyWorkingHour.overtime);
				}
				
				return memo;
			}, intial);
			
		},
			
		/**
		 * To get daily total, overtime and evening working hours 
		 * @param dailySlots, array
		 * @return 
		 */
		getDailyWorkingHours : function(dailySlots){
			return getDailyWorkingHours(dailySlots);
		}
		
		
	};
	
	

