var workingHoursCalculator = require('./workingHoursCalculator.js'),
    strategy = require('../algorithms/wageCalculationStrategies.js'),
    dateUtil = require('../utils/dateUtil.js'),
	params = require('../params.js')(),
    WageReport = require('../documents/wageReport.js'),
	moment = require('moment'),
	_ = require('underscore');
/**
 *This module get wage report
 */

module.exports = {
	/**
	 * get wage report
	 * @param: array of the monthly workslots of the same person (same ID)
	 * @returns: wage report inlcuding ID, name, total wage, evening compensation and overtime compensation
	 */
	getWageReport :function(slots){
		
		var workingHour = workingHoursCalculator.getWorkingHours(slots);
		var regularWage = strategy.getRegularWage(params.hourlyWage,workingHour.total);
		var eveningCompensation = strategy.getEveningCompensation(params.evening.compensation, workingHour.evening);
		var overtimeSlots = workingHour.overtime || [];
		var overtimeCompensation = _.reduce(overtimeSlots, function(memo, slot){
			
			//find if the overtime is longer than 4, 2 or 0
			var overtimeParam = _.find(params.overtime, function(item){
				return slot > item.hours;
			});
			var factor = overtimeParam.rate || 0;

			var overtimeCompensation = strategy.getOvertimeCompensation(params.hourlyWage, slot, factor);
			memo+=overtimeCompensation;
			return memo;
		},0.00) || 0.00;
		
		var wage = regularWage +eveningCompensation+overtimeCompensation;
		// var month = dateUtil.getMonth(slots[0].date, 'MM/YYYY');
		return new WageReport(slots[0].id, slots[0].name,params.currency, wage, eveningCompensation, overtimeCompensation);
	}
}