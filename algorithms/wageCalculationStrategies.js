var numberUtil = require('../utils/numberUtil.js');

/**
 *This class defines a set of functions to calculate wages
 */
var WageCalculationStrategy = function(){
	
	/**
	 *This function culculates regular wage for duration in hours
	 *@params hourlyWage, duration: both are numbers
	 *@return number : regular wage
 	 */
	this.getRegularWage = function(hourlyWage, duration){
		
		return numberUtil.getRoundedNumber(hourlyWage*duration);
	}
	
	/**
	 *This function culculates evening compensation for duration in hours
	 *@params eveningCompensation, duration: all are numbers, evening compensation does not include hourly wage
	 *@return number : evening work compensation
 	 */
	this.getEveningCompensation = function( eveningCompensation, duration){
	
		return numberUtil.getRoundedNumber(eveningCompensation*duration);
	}
	/**
	 *This function culculates overtime compensation for duration in hours
	 *@params hourlyWage, duration, factor: all numbers
	 *@return number : overtime compensation
 	 */
	this.getOvertimeCompensation = function(hourlyWage, duration, percentage){
		return numberUtil.getRoundedNumber(hourlyWage*duration*percentage);
	}
	
	
	
}
module.exports = new WageCalculationStrategy();