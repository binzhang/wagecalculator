var	util = require('../utils/numberUtil.js');
/**
 * wage report class construct
 */
var WageReport = function(id,name,currency,wage,eveningCompensation, overtimeCompensation){
	this.id=id;
	this.name = name;
	this.wage = util.getRoundedNumber(wage);
	this.eveningCompensation=util.getRoundedNumber(eveningCompensation);
	this.overtimeCompensation = util.getRoundedNumber(overtimeCompensation);
	this.currency = currency;
	
	
}
WageReport.prototype.toString = function(){
		return this.id + ',' + this.name+','+this.currency+this.wage+','+this.currency+this.eveningCompensation+','+this.currency+this.overtimeCompensation;
};


module.exports=WageReport;