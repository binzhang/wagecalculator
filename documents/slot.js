var params = require('../params.js')();

var column = params.csv.column;
//Slot factory class to create a slot object based on line and properties
var Slot = function(line, firstRow){
	var properties = firstRow.split(',');
	var vals = line.split(',');
	for(var i=0; i<properties.length; i++){
	  	if(properties[i] == column.name){
	  		this.name = vals[i];
	  	}else if(properties[i] == column.id){
	  		this.id = vals[i];
	  	}else if(properties[i] == column.date){
	  		this.date = vals[i];
	  	}else if(properties[i] == column.start){
	  		this.start = vals[i];
	  	}else if(properties[i] == column.end){
	  		this.end = vals[i];
	  	}
	}
};

module.exports = Slot;