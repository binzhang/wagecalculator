var should = require('chai').should(),
 	workingHoursCalculator = require('../calculators/workingHoursCalculator.js');

	describe('workingHoursCalculator', function(){
		describe('#getDailyWorkingHours', function(){
			it('should returns 1 hour as evening hours,  10.5 hours as total working hours and 2.5 as overtime hours', function(){
		        var slots=[{ 
		   		     name: 'Scott Scala',
		         	 id: '2',
		          	 date: '28.3.2014',
		          	 start: '8:30',
		   		 	 end: '19:00' } ];
		   		 var dailyWorkingHours = workingHoursCalculator.getDailyWorkingHours(slots);
				 dailyWorkingHours.should.have.property('total', 10.5);
				 dailyWorkingHours.should.have.property('evening', 1);
				 dailyWorkingHours.should.have.property('overtime', 2.5);
				 
			});
			it('should returns 0 hour as evening hours,  8 hours as total working hours and 0 as overtime hours', function(){
		        var slots=[{ name: 'Scott Scala',
  			  				 id: '2',
  			  				 date: '27.3.2014',
 			   				 start: '9:00',
							 end: '17:00' }];
		   		 var dailyWorkingHours = workingHoursCalculator.getDailyWorkingHours(slots);
				 dailyWorkingHours.should.have.property('total', 8);
				 dailyWorkingHours.should.have.property('evening', 0);
				 dailyWorkingHours.should.have.property('overtime', 0);
				 
			});
			it('should returns 0 hour as evening hours,  0.75 hours as total working hours and 0 as overtime hours', function(){
		        var slots=[{name: 'Scott Scala',
  			  				id: '2',
  			  				date: '23.3.2014',
  			  				start: '14:00',
 			   				end: '14:30' },
						  { name: 'Scott Scala',
  			  				id: '2',
  			  				date: '23.3.2014',
  			  				start: '15:00',
							end: '15:15' }];
		   		 var dailyWorkingHours = workingHoursCalculator.getDailyWorkingHours(slots);
				 dailyWorkingHours.should.have.property('total', 0.75);
				 dailyWorkingHours.should.have.property('evening', 0);
				 dailyWorkingHours.should.have.property('overtime', 0);
				 
			});
			it('should returns 1 hour as evening hours,  9 hours as total working hours and 1 as overtime hours', function(){
		        var slots=[
				  	  { name: 'Scott Scala',
			  		 	id: '2',
			  		 	date: '10.3.2014',
			  		 	start: '8:15',
			  		 	end: '16:15' },
					  { name: 'Scott Scala',
			  		 	id: '2',
			  		 	date: '10.3.2014',
			  		 	start: '22:00',
						end: '23:00' }
						];
		   		 var dailyWorkingHours = workingHoursCalculator.getDailyWorkingHours(slots);
				 dailyWorkingHours.should.have.property('total', 9);
				 dailyWorkingHours.should.have.property('evening', 1);
				 dailyWorkingHours.should.have.property('overtime', 1);
				 
			});
		  
     
		});
		describe('#getWorkingHours', function(){
			it('should return 28.25 as total, 2 as evening, 3.5 as overtime', function(){
				var slots = [{ name: 'Scott Scala',
			  		 	id: '2',
			  		 	date: '10.3.2014',
			  		 	start: '8:15',
			  		 	end: '16:15' },
					  { name: 'Scott Scala',
			  		 	id: '2',
			  		 	date: '10.3.2014',
			  		 	start: '22:00',
						end: '23:00' },
				      { name: 'Scott Scala',
  			  			id: '2',
  			  			date: '23.3.2014',
  			  			start: '14:00',
 			   			end: '14:30' },
					  { name: 'Scott Scala',
  			  			id: '2',
  			  			date: '23.3.2014',
  			  			start: '15:00',
						end: '15:15' },
				      { name: 'Scott Scala',
  			  			id: '2',
  			  			date: '27.3.2014',
 			   		    start: '9:00',
						end: '17:00' },
				       {name: 'Scott Scala',
		         	 	id: '2',
		          	 	date: '28.3.2014',
		          	 	start: '8:30',
						end: '19:00' }];
				
	   		 	var workingHours = workingHoursCalculator.getWorkingHours(slots);
			 	workingHours.should.have.property('total', 28.25);
			 	workingHours.should.have.property('evening', 2);
			 	workingHours.should.have.property('overtime').with.length(2);
			 	workingHours.should.have.deep.property('overtime[0]',1);
			 	workingHours.should.have.deep.property('overtime[1]',2.5);
				
				
				
			});
			
			it('should return 68.25, 2, [2,3]', function(){
				var slots=[{ name: 'Larry Lolcode',
       		 				id: '3',
       		 				date: '15.3.2014',
       		 				start: '15:30',
       		 				end: '17:15' },
     		   			  { name: 'Larry Lolcode',
       		 				id: '3',
       		 				date: '17.3.2014',
       		 				start: '8:30',
       		 				end: '15:30' },
     		   			  { name: 'Larry Lolcode',
       		 				id: '3',
       		 				date: '18.3.2014',
       		 				start: '9:00',
       		 				end: '15:45' },
     		   			  { name: 'Larry Lolcode',
       		 				id: '3',
       		 				date: '19.3.2014',
       		 				start: '8:30',
       		 				end: '15:45' },
     		   			  { name: 'Larry Lolcode',
       		 				id: '3',
       		 				date: '20.3.2014',
       		 				start: '1:00',
       		 				end: '3:00' },
     		   			  { name: 'Larry Lolcode',
       		 				id: '3',
       		 				date: '21.3.2014',
       		 				start: '6:00',
       		 				end: '17:00' },
     		   			  { name: 'Larry Lolcode',
       		 				id: '3',
       		 				date: '25.3.2014',
       		 				start: '9:00',
       		 				end: '16:00' },
     		   			  { name: 'Larry Lolcode',
       		 				id: '3',
       		 				date: '26.3.2014',
       		 				start: '9:30',
       		 				end: '17:00' },
     		   			  { name: 'Larry Lolcode',
       		 				id: '3',
       		 				date: '28.3.2014',
       		 				start: '6:00',
       		 				end: '16:00' },
     		   			  { name: 'Larry Lolcode',
       		 				id: '3',
       		 				date: '30.3.2014',
       		 				start: '8:00',
       		 				end: '16:00' }];
				  var workingHours = workingHoursCalculator.getWorkingHours(slots);
			      workingHours.should.have.property('total', 68.25);
				  workingHours.should.have.property('evening', 2);
				  workingHours.should.have.property('overtime').with.length(2);
				  workingHours.should.have.deep.property('overtime[0]',3);
				  workingHours.should.have.deep.property('overtime[1]',2);
							
			});
		});
	});