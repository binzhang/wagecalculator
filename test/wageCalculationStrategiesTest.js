var should = require('chai').should(),
	strategy = require('../algorithms/wageCalculationStrategies.js');
	
	describe('WageCalculationStrategies',function(){
		describe('#getRegularWage', function(){
			it('should return regular wage 1.00*1.15 = 1.15', function(){
				
				var regularWage = strategy.getRegularWage(1.15, 1.00);
				regularWage.should.equal(1.15);
			});
			it('should return regular wage 8.00*1.15 = 9.20', function(){
				
				var regularWage = strategy.getRegularWage(1.15, 8.00);
				regularWage.should.equal(9.20);
			});
		});
		
		describe('#getEveningCompensation', function(){
			it('should return evening compensation 1.00*(1.15) = 1.15', function(){
				
				var eveningCompensation = strategy.getEveningCompensation(1.15, 1.00, 1.15);
				eveningCompensation.should.equal(1.15);
			});
		});
		
		describe('#getOvertimeCompensation', function(){
			it('should return overtime compensation (1.15*1*0.25) = 0.29', function(){
				
				var overtimeCompensation = strategy.getOvertimeCompensation(1.15, 1 ,0.25);
				overtimeCompensation.should.equal(0.29);
			});
			
			it('should return overtime compensation (1.15*3*0.50) = 1.73', function(){
				
				var overtimeCompensation = strategy.getOvertimeCompensation(1.15, 3, 0.50);
				overtimeCompensation.should.equal(1.73);
			});
			
			it('should return overtime compensation (1.15*7*1) = 8.05', function(){
				
				var overtimeCompensation = strategy.getOvertimeCompensation(1.15, 7, 1.00);
				overtimeCompensation.should.equal(8.05);
			});
		});
	});