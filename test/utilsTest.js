var should = require('chai').should(),
    timeUtil = require('../utils/dateUtil.js');

describe('date util', function(){
	describe('#getDuration', function(){
		it('should return 1 in hours', function(){
			var d = timeUtil.getDuration('10.3.2014','22:00','23:00');
			d.should.equal(1);
		});
		it('should return 1.5 in hours', function(){
			var d = timeUtil.getDuration('10.3.2014','22:00','23:30');
			d.should.equal(1.5);
		});
		it('should return 1.25 in hours', function(){
			var d = timeUtil.getDuration('10.3.2014','22:00','23:15');
			d.should.equal(1.25);
		});
		it('should return 2.25 in hours', function(){
			var d = timeUtil.getDuration('10.3.2014','22:00','00:15');
			d.should.equal(2.25);
		});
		it('should return 0.25', function(){
			
			var d = timeUtil.getDuration('6.3.2014','12:30','12:45');
			d.should.equal(0.25);
		});
		
		it('should return 0.75 12:30-13:15', function(){
			var d = timeUtil.getDuration('6.3.2014','12:30','13:15');
			d.should.equal(0.75);
		});
	});
	
	describe('#getOverTimeDuration', function(){
		it('should return 0', function(){
			var d = timeUtil.getOverTimeDuration(6);
			d.should.equal(0);
		});
		it('should return 1', function(){
			var d = timeUtil.getOverTimeDuration(9);
			d.should.equal(1);
		});
	});
	
	describe('#getEveningHours', function(){
		it('should return 0, 6:00-14:00', function(){
			var d = timeUtil.getEveningHours('2.3.2014','6:00','14:00');
			d.should.equal(0);
		});
		it('should return 0 9:30-17:00', function(){
			var d = timeUtil.getEveningHours('3.3.2014','9:30','17:00');
			d.should.equal(0);
		});
		it('should return 4 16:00-22:00', function(){
			var d = timeUtil.getEveningHours('6.3.2014','16:00','22:00');
			d.should.equal(4);
		});
		it('should return 1 5:00-10:00', function(){
			var d = timeUtil.getEveningHours('6.3.2014','5:00','10:00');
			d.should.equal(1);
		});
		
		it('should return 8 16:00-2:00', function(){
			var d = timeUtil.getEveningHours('6.3.2014','16:00','2:00');
			d.should.equal(8);
		});
		it('should return 0 12:30-12:45', function(){
			var d = timeUtil.getEveningHours('6.3.2014','12:30','12:45');
			d.should.equal(0);
		});
		it('should return 7.25 14:00-01:15', function(){
			var d = timeUtil.getEveningHours('6.3.2014','14:00','01:15');
			d.should.equal(7.25);
		});
		it('should return 4 2:00-06:00', function(){
			var d = timeUtil.getEveningHours('6.3.2014','2:00','6:00');
			d.should.equal(4);
		});
		it('should return 1 10:00-19:00', function(){
			var d = timeUtil.getEveningHours('6.3.2014','10:00','19:00');
			d.should.equal(1);
		});
		it('should return 2 01:00-03:00', function(){
			var d = timeUtil.getEveningHours('6.3.2014','1:00','3:00');
			d.should.equal(2);
		});
		it('should return 0 10:00-18:00', function(){
			var d = timeUtil.getEveningHours('6.3.2014','10:00','18:00');
			d.should.equal(0);
		});
		it('should return 0.5 9:30-18:30', function(){
			var d = timeUtil.getEveningHours('6.3.2014','9:30','18:30');
			d.should.equal(0.5);
		});
	});
});