	
var _ = require('underscore'),
    Slot = require('../documents/slot.js'),
	moment = require('moment'),
	should = require('chai').should(),
    dateUtil = require('../utils/dateUtil.js'),
    LineByLineReader = require('line-by-line'),
    wageCalculator = require('../calculators/wageCalculator.js');

	describe('readTest', function(){
		describe('#lineByLine', function(){
			it('should parse the file line by line', function(done){
			    var lr = new LineByLineReader(__dirname+'/../csv/HourList201403.csv');
				
				lr.on('error', function(err){
					err.should.not.exist(err);
				});
				lr.on('line', function(line){
					line.should.be.a('string');	
					
				});
				lr.on('end', function(){
					
					
					done();
					
				});
			});
		});
	});

	describe('load test 100 employees, skipped', function(){
		describe('#getWageReport', function(){
			var firstRow = 'Person Name,Person ID,Date,Start,End',
			    slots = [];
			before(function(){
				//generate comma separated lines
				var ids = [],
				    date = [],
					start = '8:00',
					end = '16:00',
					n = 100;
				for(var i=0; i<n; i++){
					ids.push(i++);
				}
				var today = '01-03-2014',
					dateMoment = moment(today,'DD-MM-YYYY');
				for(var i=0; i<31; i++){
					
					var day =dateMoment.add(1,'days').format('DD-MM-YYYY');
					date.push(day);
					
				}
				
				for(var i=0; i<n;i++){
					for(var j=0; j<31; j++){
						var line = 'Nick,'+i+','+date[j]+','+start+','+end;
						var line1 = 'Nick,'+i+','+date[j]+',18:00,20:00';
						
						slots.push(new Slot(line, firstRow));
						slots.push(new Slot(line1, firstRow));
						
					}
				}
				
				
			});
			it.skip('check proficiency', function(){
				this.timeout(0);
				var groupedSlots = _.groupBy(slots, function(slot){
					return slot.id;
				});
				var ids = _.keys(groupedSlots);
				var month = dateUtil.getMonth(slots[0].date, 'MM/YYYY');
				console.log('Monthly Wages '+ month+':');
				console.log('id,name,total wage,evening compensation,overtime compensation');
				_.map(ids, function(id){
					var report = wageCalculator.getWageReport(groupedSlots[id]);
					console.log(report.toString());
				});
				
			});
		});
	});