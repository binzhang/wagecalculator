var should = require('chai').should(),
    wageCalculator = require('../calculators/wageCalculator.js');

describe('wageCalculator', function(){
	describe('#getWageReport', function(){
		it('should return 3, Larry Lolcode, 2.30, 7.51, 265.75 ', function(){
			
var slots=[{ name: 'Larry Lolcode',
			id: '3',
			date: '15.3.2014',
			start: '15:30',
			end: '17:15' },
		  { name: 'Larry Lolcode',
			id: '3',
			date: '17.3.2014',
			start: '8:30',
			end: '15:30' },
		  { name: 'Larry Lolcode',
			id: '3',
			date: '18.3.2014',
			start: '9:00',
			end: '15:45' },
		  { name: 'Larry Lolcode',
			id: '3',
			date: '19.3.2014',
			start: '8:30',
			end: '15:45' },
		  { name: 'Larry Lolcode',
			id: '3',
			date: '20.3.2014',
			start: '1:00',
			end: '3:00' },
		  { name: 'Larry Lolcode',
			id: '3',
			date: '21.3.2014',
			start: '6:00',
			end: '17:00' },
		  { name: 'Larry Lolcode',
			id: '3',
			date: '25.3.2014',
			start: '9:00',
			end: '16:00' },
		  { name: 'Larry Lolcode',
			id: '3',
			date: '26.3.2014',
			start: '9:30',
			end: '17:00' },
		  { name: 'Larry Lolcode',
			id: '3',
			date: '28.3.2014',
			start: '6:00',
			end: '16:00' },
		  { name: 'Larry Lolcode',
			id: '3',
			date: '30.3.2014',
			start: '8:00',
			end: '16:00' }];
			var report = wageCalculator.getWageReport(slots);
			report.should.have.property('id', '3');
			report.should.have.property('name','Larry Lolcode');
			report.should.have.property('eveningCompensation',2.30);
			report.should.have.property('overtimeCompensation',7.51);
			report.should.have.property('wage',265.75);
			
			
			
		});
	});
});