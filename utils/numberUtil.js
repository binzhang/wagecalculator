module.exports = {
	/**
	 * Round input n to 2 decimals after floating point
	 * @param n {number}
	 * @returns {Number}
	 */
	getRoundedNumber : function(n){
		return (Math.round(n*100))/100;
	}
}