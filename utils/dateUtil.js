var moment = require('moment'),
	numberUtil = require('./numberUtil.js'),
	params = require('../params.js')();

var HOUR = 1*60,//there are 60 minutes in an hour
    MAX_WORKING_HOUR = params.regularWorkingHours,
    EVE_FROM = params.evening.start,
    EVE_UNTIL = params.evening.end,
    DATE_TIME_FORMAT = params.dateTimeFormat,
    TIME_FORMAT = params.timeFormat;
	
var getDuration = function(date, start, end){
		var from = moment(date+'T'+start, DATE_TIME_FORMAT);
		var until = moment(date+'T'+end, DATE_TIME_FORMAT)
		if(until.isBefore(from)){
			until.add(1, 'days');
		}
		var duration = until.diff(from, 'm');
		return numberUtil.getRoundedNumber(duration/HOUR);
	
}

/**
 *This module defines methods that returns total working hours, evening working hours and overtime hours in a slot
 */
module.exports = {
	/**
	 * This function gets the time difference in hours between start and end
	 * @param date 
	 * @param start
	 * @param end
	 * @returns {number}, duration between start and end in hours
	 */
	getDuration : function(date, start, end){
		return getDuration(date, start, end);
	},
	/**
	 * This function checks if start and end contains evening hours
	 * @param date
	 * @param start
	 * @param end
	 * @returns {number}, number of evening hours or 0
	 */
	getEveningHours: function(date, start, end){
		var from = moment(date+'T'+start, DATE_TIME_FORMAT);
		var until = moment(date+'T'+end, DATE_TIME_FORMAT);
		if(until.isBefore(from)){
			until.add(1, 'days');
		}
		var eveFrom = moment(date+'T'+EVE_FROM, DATE_TIME_FORMAT);
		var eveUntil = moment(date+'T'+EVE_UNTIL, DATE_TIME_FORMAT);
		eveUntil.add(1, 'days');
		var eveUntilSameDay = moment(date+'T'+EVE_UNTIL, DATE_TIME_FORMAT);
		
		if(from.isBefore(eveFrom)){
			if(until.isAfter(eveFrom) && until.isBefore(eveUntil)){
				return getDuration(date, eveFrom.format(TIME_FORMAT), until.format(TIME_FORMAT));
			}else if(until.isAfter(eveFrom) && until.isAfter(eveUntil)){
				return getDuration(date, eveFrom.format(TIME_FORMAT), eveUntil.format(TIME_FORMAT));
				
			}
		}else{
			if(until.isBefore(eveUntil)){
				return getDuration(date, from.format(TIME_FORMAT), until.format(TIME_FORMAT));
				
			}else{
				return getDuration(date, from.format(TIME_FORMAT), eveUntil.format(TIME_FORMAT));
			}
		}
		
		if(from.isBefore(eveUntilSameDay)){
			if(until.isAfter(eveUntilSameDay)){
				return getDuration(date, from.format(TIME_FORMAT), eveUntilSameDay.format(TIME_FORMAT));
				
			}else{
				return getDuration(date, from.format(TIME_FORMAT), until.format(TIME_FORMAT));
				
			}
		}
		return 0;
		
	},
	/**
	 * This function calculates the number of overtime working hours
	 * @param duration {number}, the number of total working hours in a day
	 * @returns {number}, the number of overtime working hours
	 */
	getOverTimeDuration: function(duration){
		return duration > MAX_WORKING_HOUR ? (duration-MAX_WORKING_HOUR):0;
	},
	/**
	 *get the month the date is in in the format of 03/2014
	 *@param date: string, 01-03-2015
	 *@param format: string , e.g. MM/YYYY
	 *@return the month and year string in the format provided by format param
	 */
	getMonth: function(date, format){
		return moment(date, params.dateFormat).format(format);
	}
}